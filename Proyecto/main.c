#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>

void gotoxy(int x, int y){

    COORD coord;
    coord.X = x;
    coord.Y = y;

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);

}

int main(){

    gotoxy(10,2); printf("GALINDO MENDOZA GEOVANNI ISRAEL - 1EV2");

    int opcion, option;

    do {

        gotoxy(5,5); printf("CUESTIONARIO DE CONOCIMIENTOS GENERALES");
        gotoxy(2,8); printf("1 - MATEMATICAS GENERALES");
        gotoxy(2,9); printf("2 - FISICA");
        gotoxy(2,10); printf("3 - BIOLOGIA");
        gotoxy(2,11); printf("4 - QUIMICA");
        gotoxy(2,12); printf("5 - Salir");

        gotoxy(2,14); printf("ELIJA UNA OPCION:");
        gotoxy(20,14); scanf("%d",&opcion);

        switch(opcion){

            case 1:
            {
                gotoxy(3,18); printf("1) Dos numeros que sumados den 15 y restados den 3");
                        gotoxy(3,19); printf("1. 10 y 5");
                        gotoxy(3,20); printf("2. 20 y -5");
                        gotoxy(3,21); printf("3. 9 y 6");
                        gotoxy(3,22); printf("4. 8 y 7");

                        gotoxy(3,23); printf("Opcion:");
                        gotoxy(11,23); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,25); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,25); printf("INCORRECTO");
                    }

                gotoxy(3,27); printf("2) Que tipo de numero representa la letra Q");
                        gotoxy(3,28); printf("1. Reales");
                        gotoxy(3,29); printf("2. Racionales");
                        gotoxy(3,30); printf("3. Complejos");
                        gotoxy(3,31); printf("4. Enteros");

                        gotoxy(3,32); printf("Opcion:");
                        gotoxy(11,32); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,34); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,34); printf("INCORRECTO");
                    }

                gotoxy(3,36); printf("3) El siguiente termino 3x^2 representa un:");
                        gotoxy(3,37); printf("1. Termino Cuadratico");
                        gotoxy(3,38); printf("2. Termino lineal");
                        gotoxy(3,39); printf("3. Termino Parabolico");
                        gotoxy(3,40); printf("4. Ninguna de las anteriores");

                        gotoxy(3,41); printf("Opcion:");
                        gotoxy(11,41); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,43); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,43); printf("INCORRECTO");
                    }

                gotoxy(3,45); printf("4) Cuanto es el 30 por ciento de 1245:");
                        gotoxy(3,46); printf("1. 373.48");
                        gotoxy(3,47); printf("2. 373.4");
                        gotoxy(3,48); printf("3. 373.34");
                        gotoxy(3,49); printf("4. 373.5");

                        gotoxy(3,51); printf("Opcion:");
                        gotoxy(11,51); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,53); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,53); printf("INCORRECTO");
                    }

                gotoxy(3,55); printf("5) La raiz cuadrada de -4 es: ");
                        gotoxy(3,56); printf("1. 2 ");
                        gotoxy(3,57); printf("2. -2");
                        gotoxy(3,58); printf("3. +2 y -2");
                        gotoxy(3,59); printf("4. No tiene solucion");

                        gotoxy(3,61); printf("Opcion:");
                        gotoxy(11,61); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,63); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,63); printf("INCORRECTO");
                    }

                gotoxy(3,65); printf("6) Como resolverias el siguiente sistema (X + Y = 15) ; (5X + 12Y = -17): ");
                        gotoxy(3,66); printf("1. Metodo de eliminacion o mas menos");
                        gotoxy(3,67); printf("2. Metodo Matrices");
                        gotoxy(3,68); printf("3. Metodo de Sustitucion");
                        gotoxy(3,69); printf("4. Metodo de Igualacion");
                        gotoxy(3,70); printf("5. Cualquiera de las anteriores");

                        gotoxy(3,72); printf("Opcion:");
                        gotoxy(11,72); scanf("%d", &option);

                    if (option == 5){
                        gotoxy(4,74); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,74); printf("CORRECTO");
                    }

                gotoxy(3,76); printf("7) Calcula el valor de X en X^2 + 2Y = 16 donde Y = 4: ");
                        gotoxy(3,77); printf("1. 4");
                        gotoxy(3,78); printf("2. 2");
                        gotoxy(3,79); printf("3. Raiz cuadrada de 8");
                        gotoxy(3,80); printf("4. 8");

                        gotoxy(3,82); printf("Opcion:");
                        gotoxy(11,82); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,84); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,84); printf("INCORRECTO");
                    }

                gotoxy(3,86); printf("8) Calcula el valor de X en la siguiente expresion X^2 + 2X + 1 = 16: ");
                        gotoxy(3,87); printf("1. X = -1");
                        gotoxy(3,88); printf("2. X = 0 y X = -1");
                        gotoxy(3,89); printf("3. X = +1");
                        gotoxy(3,90); printf("4. X = -5 y X = +3");

                        gotoxy(3,92); printf("Opcion:");
                        gotoxy(11,92); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,94); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,94); printf("INCORRECTO");
                    }

                gotoxy(3,96); printf("9) Calcula la derivada de X^3 + 5X^2 + X ");
                        gotoxy(3,97); printf("1. 3X^3 + 10X + 1");
                        gotoxy(3,98); printf("2. 3X^2 + 10X + 1");
                        gotoxy(3,99); printf("3. 3X^4 + 10");
                        gotoxy(3,100); printf("4. 3X^2 - 10X + 1");

                        gotoxy(3,102); printf("Opcion:");
                        gotoxy(11,102); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,104); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,104); printf("INCORRECTO");
                    }

                gotoxy(3,106); printf("10) Calcula la derivada de 12X^12 - 15X^7 - 2");
                        gotoxy(3,107); printf("1. 144X^11 + 105X - 2");
                        gotoxy(3,108); printf("2. 144X^11 - 105X^5 -2");
                        gotoxy(3,109); printf("3. 144X^11 - 105X^6 - 0");
                        gotoxy(3,110); printf("4. 144X^11 - 105X^6 - 2");

                        gotoxy(3,112); printf("Opcion:");
                        gotoxy(11,112); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,114); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,114); printf("INCORRECTO");
                    }

                gotoxy(3,116); printf("11) Calcula la derivada de 12X^5 - 15X^4 - 10X^3 + 2X^2 - 7X - 20");
                        gotoxy(3,117); printf("1. 60X^4 - 60X^2 + 30X^2 + 4X^2 - 7X - 0");
                        gotoxy(3,118); printf("2. 60X^4 - 60X^3 - 30X^2 + 4X - 7 - 0");
                        gotoxy(3,119); printf("3. 60X^4 + 60X^3 - 30X^2 - 4X - 7X - 0");
                        gotoxy(3,120); printf("4. 60X^4 - 60X^3 - 30X^2 + 4X^2 - 7 - 0");

                        gotoxy(3,122); printf("Opcion:");
                        gotoxy(11,122); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,124); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,124); printf("INCORRECTO");
                    }

                gotoxy(3,126); printf("12) Calcula la derivada de (X^3)^2 + X^5 - (2X)^3 = 12X");
                        gotoxy(3,127); printf("1. 2(X^3) + 5X^4 - (2X)^2 = 12");
                        gotoxy(3,128); printf("2. 2(X^3) + 5X^6 + 3(2X)^2 = 12");
                        gotoxy(3,129); printf("3. 6X^5 + 5X^4 - 24X^2 -  12 = 0");
                        gotoxy(3,130); printf("4. 2(X^3) - 5X^5 - (2X)^3 = 12");

                        gotoxy(3,132); printf("Opcion:");
                        gotoxy(11,132); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,134); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,134); printf("INCORRECTO");
                    }

                gotoxy(3,136); printf("13) Obten el valor de X, usa el criterio de la 1ra derivada de X^3 - 2X^2");
                        gotoxy(3,137); printf("1. X = 0 y X = -4/3");
                        gotoxy(3,138); printf("2. X = +4/3 y X = -4/3");
                        gotoxy(3,139); printf("3. X = -4/3 y X = -3/4");
                        gotoxy(3,140); printf("4. X = +3/4 y X = -4/3");

                        gotoxy(3,142); printf("Opcion:");
                        gotoxy(11,142); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,144); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,144); printf("INCORRECTO");
                    }

                gotoxy(3,146); printf("14) Calcula la antiderivada de 20X^2 - 2");
                        gotoxy(3,147); printf("1. (20X^3)/3 + 2X - C");
                        gotoxy(3,148); printf("2. (20X^3)/3 - 2X + C");
                        gotoxy(3,149); printf("3. (20X^4)/3 - 2X");
                        gotoxy(3,150); printf("4. (20X^3)/4 - 2X");

                        gotoxy(3,152); printf("Opcion:");
                        gotoxy(11,152); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,154); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,154); printf("INCORRECTO");
                    }

                gotoxy(3,156); printf("15) Calcula la antiderivada de tanX + 1");
                        gotoxy(3,157); printf("1. LnCscX + X + C");
                        gotoxy(3,158); printf("2. LnCosX + X + C");
                        gotoxy(3,159); printf("3. -LnSecX + X + C");
                        gotoxy(3,160); printf("4. -LnCosX + X + C");

                        gotoxy(3,162); printf("Opcion:");
                        gotoxy(11,162); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,164); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,164); printf("INCORRECTO");
                    }
                gotoxy(4,166); system("pause");
                system("cls");
                break;
            }


            case 2:
            {
                gotoxy(3,18); printf("1) Son magnitudes fundamentales: ");
                        gotoxy(3,19); printf("1. Corriente electrica y Presion");
                        gotoxy(3,20); printf("2. Optica y Distancia");
                        gotoxy(3,21); printf("3. Temperatura y Cantidad de sustancia");
                        gotoxy(3,22); printf("4. Tiempo y Masa");

                        gotoxy(3,23); printf("Opcion:");
                        gotoxy(11,23); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,25); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,25); printf("INCORRECTO");
                    }

                gotoxy(3,27); printf("2) La unidad de la fuerza es:");
                        gotoxy(3,28); printf("1. (kg * m) / s^2");
                        gotoxy(3,29); printf("2. kg * m * s^2");
                        gotoxy(3,30); printf("3. s^2 / (kg * m)");
                        gotoxy(3,31); printf("4. m / (s^2 * kg)");

                        gotoxy(3,32); printf("Opcion:");
                        gotoxy(11,32); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,34); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,34); printf("INCORRECTO");
                    }

                gotoxy(3,36); printf("3) La unidad de la intensidad luminosa es:");
                        gotoxy(3,37); printf("1. Lumen");
                        gotoxy(3,38); printf("2. Lux");
                        gotoxy(3,39); printf("3. Candela");
                        gotoxy(3,40); printf("4. Ampere");

                        gotoxy(3,41); printf("Opcion:");
                        gotoxy(11,41); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,43); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,43); printf("INCORRECTO");
                    }

                gotoxy(3,45); printf("4) �Que estudia principalmente la fisica?");
                        gotoxy(3,46); printf("1. Materia y Espacio");
                        gotoxy(3,47); printf("2. Materia y Energia");
                        gotoxy(3,48); printf("3. Espacio y Tiempo");
                        gotoxy(3,49); printf("4. Tiempo y Energia");

                        gotoxy(3,51); printf("Opcion:");
                        gotoxy(11,51); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,53); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,53); printf("INCORRECTO");
                    }

                gotoxy(3,55); printf("5)  Son magnitudes derivadas: ");
                        gotoxy(3,56); printf("1. Tiempo y Masa");
                        gotoxy(3,57); printf("2. cantidad de sustancia y Peso");
                        gotoxy(3,58); printf("3. Area y Corriente electrica");
                        gotoxy(3,59); printf("4. Volumen y Fuerza");

                        gotoxy(3,61); printf("Opcion:");
                        gotoxy(11,61); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,63); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,63); printf("INCORRECTO");
                    }

                gotoxy(3,65); printf("6) Caracteristica del vector que indica hacia donde se aplica la fuerza");
                        gotoxy(3,66); printf("1. Direccion");
                        gotoxy(3,67); printf("2. Magnitud");
                        gotoxy(3,68); printf("3. Punto de Aplicacion");
                        gotoxy(3,69); printf("4. Sentido");
                        gotoxy(3,70); printf("5. Unidad");

                        gotoxy(3,72); printf("Opcion:");
                        gotoxy(11,72); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,74); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,74); printf("CORRECTO");
                    }

                gotoxy(3,76); printf("7) Son metodos utilizados para resolver sistemas vectoriales");
                        gotoxy(3,77); printf("1. Poligono y Cubico");
                        gotoxy(3,78); printf("2. Paralelogramo y Paralelepipedo");
                        gotoxy(3,79); printf("3. Poligono y Paralelogramo");
                        gotoxy(3,80); printf("4. Cubico y Paralelepipedo");

                        gotoxy(3,82); printf("Opcion:");
                        gotoxy(11,82); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,84); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,84); printf("INCORRECTO");
                    }

                gotoxy(3,86); printf("8) �Para Einstein �Que era la gravedad?");
                        gotoxy(3,87); printf("1. Una Fuerza");
                        gotoxy(3,88); printf("2. Una Curvatura");
                        gotoxy(3,89); printf("3. Una Dilatacion");
                        gotoxy(3,90); printf("4. Una Atraccion");

                        gotoxy(3,92); printf("Opcion:");
                        gotoxy(11,92); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,94); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,94); printf("INCORRECTO");
                    }

                gotoxy(3,96); printf("9) �Que magnitudes se consideraron en la Ley de Coulomb?");
                        gotoxy(3,97); printf("1. Constante de coulomb, Carga Electrica y Distancia");
                        gotoxy(3,98); printf("2. Constante de coulomb, Masas y Distancia");
                        gotoxy(3,99); printf("3. Constante Gravitacional, Carga Electrica y Distancia");
                        gotoxy(3,100); printf("4. Numero de Avogadro, Carga Electrica y Distancia");

                        gotoxy(3,102); printf("Opcion:");
                        gotoxy(11,102); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,104); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,104); printf("INCORRECTO");
                    }

                gotoxy(3,106); printf("10) �Que es la capacitancia?");
                        gotoxy(3,107); printf("1. Es la energia almacenada en forma de calor");
                        gotoxy(3,108); printf("2. Es la energia almacenada en forma de campo magnetico");
                        gotoxy(3,109); printf("3. Es la energia almacenada en forma de fuerza");
                        gotoxy(3,110); printf("4. Es la energia almacenada en forma de luz");

                        gotoxy(3,112); printf("Opcion:");
                        gotoxy(11,112); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,114); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,114); printf("INCORRECTO");
                    }

                gotoxy(3,116); printf("11) Definicion de dinamica:");
                        gotoxy(3,117); printf("1. Estudio de los cuerpos en reposo");
                        gotoxy(3,118); printf("2. Estudio de los cuerpos inerciales");
                        gotoxy(3,119); printf("3. Estudio de los cuerpos sonicos");
                        gotoxy(3,120); printf("4. Estudio de los cuerpos en movimiento");

                        gotoxy(3,122); printf("Opcion:");
                        gotoxy(11,122); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,124); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,124); printf("INCORRECTO");
                    }

                gotoxy(3,126); printf("12) �Que menciona la ley de weber");
                        gotoxy(3,127); printf("1. Los objetos estan compuestos de imanes peque�os");
                        gotoxy(3,128); printf("2. Todos tenemos magnetismo");
                        gotoxy(3,129); printf("3. Los objetos producen magnetismo");
                        gotoxy(3,130); printf("4. Los cuerpos no alineados no tienen magnetismo");

                        gotoxy(3,132); printf("Opcion:");
                        gotoxy(11,132); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,134); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,134); printf("INCORRECTO");
                    }

                gotoxy(3,136); printf("13) Que unidades se ocupan para definir la distancia en el espacio");
                        gotoxy(3,137); printf("1. Meses luz");
                        gotoxy(3,138); printf("2. Dias luz");
                        gotoxy(3,139); printf("3. A�os luz");
                        gotoxy(3,140); printf("4. Siglos luz");

                        gotoxy(3,142); printf("Opcion:");
                        gotoxy(11,142); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,144); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,144); printf("INCORRECTO");
                    }

                gotoxy(3,146); printf("14) �Que leyes explican el movimientos de los cuerpos en el espacio?");
                        gotoxy(3,147); printf("1. Leyes de Kepler");
                        gotoxy(3,148); printf("2. Leyes de Copernico");
                        gotoxy(3,149); printf("3. Leyes de Galileo");
                        gotoxy(3,150); printf("4. Leyes de Newton");

                        gotoxy(3,152); printf("Opcion:");
                        gotoxy(11,152); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,154); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,154); printf("INCORRECTO");
                    }

                gotoxy(3,156); printf("15) Los agujeros negros son:");
                        gotoxy(3,157); printf("1. Son considerados soles negros");
                        gotoxy(3,158); printf("2. Son regiones en el espacion con gran masa");
                        gotoxy(3,159); printf("3. Son regiones en el espacion con alta gravedad");
                        gotoxy(3,160); printf("4. Todas las anteriores");

                        gotoxy(3,162); printf("Opcion:");
                        gotoxy(11,162); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,164); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,164); printf("INCORRECTO");
                    }
                gotoxy(4,166); system("pause");
                system("cls");
                break;
            }


            case 3: //BIOLOGIA
            {
                gotoxy(3,18); printf("1) Es la capacidad de los seres vivos a reacondicionarse al medio:");
                        gotoxy(3,19); printf("1. Reproduccion");
                        gotoxy(3,20); printf("2. Homeostasis");
                        gotoxy(3,21); printf("3. Adpatabilidad");
                        gotoxy(3,22); printf("4. Irritabilidad");

                        gotoxy(3,23); printf("Opcion:");
                        gotoxy(11,23); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,25); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,25); printf("INCORRECTO");
                    }

                gotoxy(3,27); printf("2) Capacidad de los seres vivos de mantener su equilibrio biologico");
                        gotoxy(3,28); printf("1. Homesotasis");
                        gotoxy(3,29); printf("2. Irritabilidad");
                        gotoxy(3,30); printf("3. Crecimiento");
                        gotoxy(3,31); printf("4. Metabolismo");

                        gotoxy(3,32); printf("Opcion:");
                        gotoxy(11,32); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,34); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,34); printf("INCORRECTO");
                    }

                gotoxy(3,36); printf("3) Involucra todas las caracteristicas de los seres vivos");
                        gotoxy(3,37); printf("1. Eritrocitos");
                        gotoxy(3,38); printf("2. Micras");
                        gotoxy(3,39); printf("3. Biap�crisis");
                        gotoxy(3,40); printf("4. Pili");

                        gotoxy(3,41); printf("Opcion:");
                        gotoxy(11,41); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,43); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,43); printf("INCORRECTO");
                    }

                gotoxy(3,45); printf("4) Son consideradas moleculas organicas:");
                        gotoxy(3,46); printf("1. Carbohidratos y Alelo");
                        gotoxy(3,47); printf("2. Ovogenesis y Lipidos");
                        gotoxy(3,48); printf("3. ADN y RNA");
                        gotoxy(3,49); printf("4. Proteinas y Mitosis");

                        gotoxy(3,51); printf("Opcion:");
                        gotoxy(11,51); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,53); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,53); printf("INCORRECTO");
                    }

                gotoxy(3,55); printf("5)  Son los dos tipos de celulas:");
                        gotoxy(3,56); printf("1. Eucariota y Procariota");
                        gotoxy(3,57); printf("2. Vegetal y Animal");
                        gotoxy(3,58); printf("3. Eucariota y Vegetal");
                        gotoxy(3,59); printf("4. Procariota y Animal");

                        gotoxy(3,61); printf("Opcion:");
                        gotoxy(11,61); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,63); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,63); printf("INCORRECTO");
                    }

                gotoxy(3,65); printf("6) Proceso de formacion en donde varia la informacion genetica");
                        gotoxy(3,66); printf("1. Division Genetica");
                        gotoxy(3,67); printf("2. Division celular");
                        gotoxy(3,68); printf("3. Mitosis");
                        gotoxy(3,69); printf("4. Division cromosomica");
                        gotoxy(3,70); printf("5. Meiosis");

                        gotoxy(3,72); printf("Opcion:");
                        gotoxy(11,72); scanf("%d", &option);

                    if (option == 5){
                        gotoxy(4,74); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,74); printf("CORRECTO");
                    }

                gotoxy(3,76); printf("7) La _____ es un proceso que genera las celulas sexuales masculinas");
                        gotoxy(3,77); printf("1. Meiosis - Esperthom");
                        gotoxy(3,78); printf("2. Meiosis - Espermatogenesis");
                        gotoxy(3,79); printf("3. Meiosis - Espermatosoides");
                        gotoxy(3,80); printf("4. Meiosis - Espermaton");

                        gotoxy(3,82); printf("Opcion:");
                        gotoxy(11,82); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,84); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,84); printf("INCORRECTO");
                    }

                gotoxy(3,86); printf("8) Son tipos de reproduccion asexual");
                        gotoxy(3,87); printf("1. Gemacion y Esporulacion");
                        gotoxy(3,88); printf("2. Gemacion y Esporacion");
                        gotoxy(3,89); printf("3. Propagulos y Portan");
                        gotoxy(3,90); printf("4. Estolones y Bacterias");

                        gotoxy(3,92); printf("Opcion:");
                        gotoxy(11,92); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,94); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,94); printf("INCORRECTO");
                    }

                gotoxy(3,96); printf("9) Rama de la biologia que estudia los caracteres de generacion genetica");
                        gotoxy(3,97); printf("1. Gen");
                        gotoxy(3,98); printf("2. Genetica");
                        gotoxy(3,99); printf("3. Genoma");
                        gotoxy(3,100); printf("4. Genacion");

                        gotoxy(3,102); printf("Opcion:");
                        gotoxy(11,102); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,104); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,104); printf("INCORRECTO");
                    }

                gotoxy(3,106); printf("10) Rasgo que se hereda, viene determinado por dos genes:");
                        gotoxy(3,107); printf("1. Caracter");
                        gotoxy(3,108); printf("2. Alelo recesivo");
                        gotoxy(3,109); printf("3. Alelo Dominante");
                        gotoxy(3,110); printf("4. F1");

                        gotoxy(3,112); printf("Opcion:");
                        gotoxy(11,112); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,114); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,114); printf("INCORRECTO");
                    }

                gotoxy(3,116); printf("11) La 1ra Ley de Mendel es tambien conocida como:");
                        gotoxy(3,117); printf("1. Ley de segregacion de genes");
                        gotoxy(3,118); printf("2. Ley de segregacion de un par de caracteres independientes");
                        gotoxy(3,119); printf("3. Ley de segregacion de caracteres");
                        gotoxy(3,120); printf("4. Ley de la segregacion de la genetica");

                        gotoxy(3,122); printf("Opcion:");
                        gotoxy(11,122); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,124); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,124); printf("INCORRECTO");
                    }

                gotoxy(3,126); printf("12) Cada clase contiene un numero de ____ relacionadas de organismos");
                        gotoxy(3,127); printf("1. Phylum");
                        gotoxy(3,128); printf("2. Orden");
                        gotoxy(3,129); printf("3. Familia");
                        gotoxy(3,130); printf("4. Reino");

                        gotoxy(3,132); printf("Opcion:");
                        gotoxy(11,132); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,134); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,134); printf("INCORRECTO");
                    }

                gotoxy(3,136); printf("13) En cuantos dominios Woese dividio a los seres vivos:");
                        gotoxy(3,137); printf("1. 2");
                        gotoxy(3,138); printf("2. 3");
                        gotoxy(3,139); printf("3. 4");
                        gotoxy(3,140); printf("4. 5");

                        gotoxy(3,142); printf("Opcion:");
                        gotoxy(11,142); scanf("%d", &option);

                    if (opcion == 3){
                        gotoxy(4,144); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,144); printf("INCORRECTO");
                    }

                gotoxy(3,146); printf("14) Es una interaccion en la que un organismo consume al otro");
                        gotoxy(3,147); printf("1. Parasitismo");
                        gotoxy(3,148); printf("2. Comensalimso");
                        gotoxy(3,149); printf("3. Amenalismo");
                        gotoxy(3,150); printf("4. Depredacion");

                        gotoxy(3,152); printf("Opcion:");
                        gotoxy(11,152); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,154); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,154); printf("INCORRECTO");
                    }

                gotoxy(3,156); printf("15) Una _____ es un conjunto de varias cadenas troficas interconectadas");
                        gotoxy(3,157); printf("1. Productores");
                        gotoxy(3,158); printf("2. Consumidores");
                        gotoxy(3,159); printf("3. Cadena Alimenticia");
                        gotoxy(3,160); printf("4. Desintegradores");

                        gotoxy(3,162); printf("Opcion:");
                        gotoxy(11,162); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,164); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,164); printf("INCORRECTO");
                    }
                gotoxy(4,166); system("pause");
                system("cls");
                break;
            }


            case 4: //QUIMICA
            {
                gotoxy(3,18); printf("1) �Que es la materia?");
                        gotoxy(3,19); printf("1. Es una sustancia");
                        gotoxy(3,20); printf("2. Es algo que ocupa un espacio y tiempo");
                        gotoxy(3,21); printf("3. Es una cantidad de sutancia");
                        gotoxy(3,22); printf("4. Es una masa");

                        gotoxy(3,23); printf("Opcion:");
                        gotoxy(11,23); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,25); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,25); printf("INCORRECTO");
                    }

                gotoxy(3,27); printf("2) En cuantas sustancias se divide la materia y cuales son:");
                        gotoxy(3,28); printf("1. 2, pura e impura");
                        gotoxy(3,29); printf("2. 2, homogenea y heterogenea");
                        gotoxy(3,30); printf("3. 2, compuesto y mezcla");
                        gotoxy(3,31); printf("4. 2, molecula y atomo");

                        gotoxy(3,32); printf("Opcion:");
                        gotoxy(11,32); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,34); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,34); printf("INCORRECTO");
                    }

                gotoxy(3,36); printf("3) Sustancia impura constituida por varias sustancias de forma fisica:");
                        gotoxy(3,37); printf("1. Compuesto");
                        gotoxy(3,38); printf("2. Atomo");
                        gotoxy(3,39); printf("3. Elemento");
                        gotoxy(3,40); printf("4. Mezcla");

                        gotoxy(3,41); printf("Opcion:");
                        gotoxy(11,41); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,43); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,43); printf("INCORRECTO");
                    }

                gotoxy(3,45); printf("4) Es todo cambio cambio a nivel interno o molecular de una sustancia:");
                        gotoxy(3,46); printf("1. Ecuacion Quimica");
                        gotoxy(3,47); printf("2. Reaccion Quimica");
                        gotoxy(3,48); printf("3. Fenomeno");
                        gotoxy(3,49); printf("4. Propiedades");

                        gotoxy(3,51); printf("Opcion:");
                        gotoxy(11,51); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,53); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,53); printf("INCORRECTO");
                    }

                gotoxy(3,55); printf("5)  Son metodos para separar mezclas:");
                        gotoxy(3,56); printf("1. Fibrilacion y Condensacion");
                        gotoxy(3,57); printf("2. Magnetismo y Circulacion");
                        gotoxy(3,58); printf("3. Cromatografia y Cristalizacion");
                        gotoxy(3,59); printf("4. Filtracion y Decancion");

                        gotoxy(3,61); printf("Opcion:");
                        gotoxy(11,61); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,63); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,63); printf("INCORRECTO");
                    }

                gotoxy(3,65); printf("6) La suma de las masas de los reactivos es igual a la suma de las masas de:");
                        gotoxy(3,66); printf("1. La produccion");
                        gotoxy(3,67); printf("2. Los productos");
                        gotoxy(3,68); printf("3. Lo generado");
                        gotoxy(3,69); printf("4. Los compuestos");
                        gotoxy(3,70); printf("5. Los elementos");

                        gotoxy(3,72); printf("Opcion:");
                        gotoxy(11,72); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,74); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,74); printf("CORRECTO");
                    }

                gotoxy(3,76); printf("7) Concluyo que los electrones se distribuyen en orbitas circulares especificas");
                        gotoxy(3,77); printf("1. Ernest Rutherford");
                        gotoxy(3,78); printf("2. Erwin Schrodinger");
                        gotoxy(3,79); printf("3. James Chadwick");
                        gotoxy(3,80); printf("4. Niels Bohr");

                        gotoxy(3,82); printf("Opcion:");
                        gotoxy(11,82); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,84); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,84); printf("INCORRECTO");
                    }

                gotoxy(3,86); printf("8) Es el elemento mas electronegativo");
                        gotoxy(3,87); printf("1. Fluor");
                        gotoxy(3,88); printf("2. Boro");
                        gotoxy(3,89); printf("3. Oxigeno");
                        gotoxy(3,90); printf("4. Cloro");

                        gotoxy(3,92); printf("Opcion:");
                        gotoxy(11,92); scanf("%d", &option);

                    if (option == 1){
                        gotoxy(4,94); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,94); printf("INCORRECTO");
                    }

                gotoxy(3,96); printf("9) Es un tipo de enlace que se lleva entre dos elementos No metalicos");
                        gotoxy(3,97); printf("1. Enlace No metalico");
                        gotoxy(3,98); printf("2. Enlace Ionico");
                        gotoxy(3,99); printf("3. Enlace Covalente");
                        gotoxy(3,100); printf("4. Enlace Dativo");

                        gotoxy(3,102); printf("Opcion:");
                        gotoxy(11,102); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,104); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,104); printf("INCORRECTO");
                    }

                gotoxy(3,106); printf("10) �Que menciona la primera ley de la termodinamica?");
                        gotoxy(3,107); printf("1. Conservacion de la materia");
                        gotoxy(3,108); printf("2. Conservacion de la energia en sistemas termodinamicos");
                        gotoxy(3,109); printf("3. Conservacion de la masa");
                        gotoxy(3,110); printf("4. Conservacion de la temperatura");

                        gotoxy(3,112); printf("Opcion:");
                        gotoxy(11,112); scanf("%d", &option);

                    if (option == 2){
                        gotoxy(4,114); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,114); printf("INCORRECTO");
                    }

                gotoxy(3,116); printf("11) Son aquellas reacciones que tienen dos sentidos:");
                        gotoxy(3,117); printf("1. Reaccion Irreversible");
                        gotoxy(3,118); printf("2. Reaccion de doble sentido");
                        gotoxy(3,119); printf("3. Reaccion Codificado");
                        gotoxy(3,120); printf("4. Reacciones Reversibles");

                        gotoxy(3,122); printf("Opcion:");
                        gotoxy(11,122); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,124); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,124); printf("INCORRECTO");
                    }

                gotoxy(3,126); printf("12) La ____ es el calculo de las relaciones cuantitativas para una ecuacion quimica");
                        gotoxy(3,127); printf("1. Balanceo Quimico");
                        gotoxy(3,128); printf("2. Nomenclatura");
                        gotoxy(3,129); printf("3. Estqeuiometria");
                        gotoxy(3,130); printf("4. Farmaceutocomia");

                        gotoxy(3,132); printf("Opcion:");
                        gotoxy(11,132); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,134); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,134); printf("INCORRECTO");
                    }

                gotoxy(3,136); printf("13) Es conocido como la particula mas peque�a de una sustancia:");
                        gotoxy(3,137); printf("1. Atomo");
                        gotoxy(3,138); printf("2. Molecula");
                        gotoxy(3,139); printf("3. Compuesto");
                        gotoxy(3,140); printf("4. Elemento");

                        gotoxy(3,142); printf("Opcion:");
                        gotoxy(11,142); scanf("%d", &option);

                    if (opcion == 1){
                        gotoxy(4,144); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,144); printf("INCORRECTO");
                    }

                gotoxy(3,146); printf("14) Es el numero de particulas constituyente que se encuentran en una mol:");
                        gotoxy(3,147); printf("1. Constante de la luz");
                        gotoxy(3,148); printf("2. Constante de Boltzman");
                        gotoxy(3,149); printf("3. Constante de Avogadro");
                        gotoxy(3,150); printf("4. Constante de hitman");

                        gotoxy(3,152); printf("Opcion:");
                        gotoxy(11,152); scanf("%d", &option);

                    if (option == 3){
                        gotoxy(4,154); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,154); printf("INCORRECTO");
                    }

                gotoxy(3,156); printf("15) Se considera a los ___ como la estructura mas ordenada:");
                        gotoxy(3,157); printf("1. Filamentos");
                        gotoxy(3,158); printf("2. Ligas");
                        gotoxy(3,159); printf("3. Plasticos");
                        gotoxy(3,160); printf("4. Cristales");

                        gotoxy(3,162); printf("Opcion:");
                        gotoxy(11,162); scanf("%d", &option);

                    if (option == 4){
                        gotoxy(4,164); printf("CORRECTO");
                    }
                    else {
                        gotoxy(4,164); printf("INCORRECTO");
                    }

                gotoxy(4,166); system("pause");
                system("cls");
                break;
            }

        }

    gotoxy(20,20);

    }

    while (opcion != 5);

    return 0;
}
